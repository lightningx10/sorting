export function main(): void {
    console.log("Starting sorting demonstrations in TypeScript");
    demo(insertionSort, 10, "Insertion");
    demo(selectionSort, 10, "Selection");
    demo(radixSort, 10, "Radix");
    demo(bogoSort, 12, "Bogo");

    const fs = require('fs');
    const loc = "results/ts.csv";
    const fileTup:[any, string] = [fs, loc];
    console.log("Starting timed sorting in TypeScript");
    initCSV(fileTup);
    benchmark(insertionSort, 1200, 10000, "Insertion", fileTup);
    benchmark(insertionSort, 5000, 10000, "Insertion", fileTup);
    benchmark(insertionSort, 12000, 10000, "Insertion", fileTup);
    benchmark(selectionSort, 1200, 10000, "Selection", fileTup);
    benchmark(selectionSort, 5000, 10000, "Selection", fileTup);
    benchmark(selectionSort, 12000, 10000, "Selection", fileTup);
    benchmark(radixSort, 1200, 10000, "Radix", fileTup);
    benchmark(radixSort, 5000, 10000, "Radix", fileTup);
    benchmark(radixSort, 12000, 10000, "Radix", fileTup);
    benchmark(bogoSort, 11, 1, "Bogo", fileTup);
    benchmark(bogoSort, 12, 1, "Bogo", fileTup);
    benchmark(bogoSort, 13, 1, "Bogo", fileTup);
    return;
}

function demo(f: (array: number[]) => void, size: number, name: string): void {
    var array = genRandArray(size);
    console.log(name + " sorting demonstration on an array of size " + size);
    console.log("Before:");
    printArray(array);
    f(array);
    console.log("After:");
    printArray(array);
    return;
}

function initCSV(fileTup: [any, string]): void {
    const [fs, loc] = fileTup;
    try {
        fs.writeFileSync(loc, "name,rep,size,time\n");
    } catch (err) {
        console.log("Failed to open/make results file");
        console.error(err);
    }
    return;
}

function benchmark(f: (array: number[]) => void, size: number, n: number, name: string, fileTup: [any, string]): void {
    var arrays = new Array(size);
    for (let i = 0; i < n; i ++) {
        arrays[i] = genRandArray(size);
    }
    var start = new Date().getTime();
    for (let i = 0; i < n; i ++) {
        f(arrays[i]);
    }
    var end = new Date().getTime();
    var time = (end - start)/1000;
    console.log("It took " + time + " seconds to do " + name + " sort " + n + " times on arrays of size " + size);
    const [fs, loc] = fileTup;
    try {
        fs.appendFileSync(loc, name + "," + n + "," + size + "," + time + "\n");
    } catch (err) {
        console.log("Failed to append to results file");
        console.error(err);
    }
    return;
}

function genRandArray(size: number): number[] {
    if (size == 0) {
        return [0];
    }
    var array = new Array(size);
    for (let i = 0; i < size; i ++) {
        array[i] = Math.floor(Math.random() * 100);
    }
    return array;
}

function printArray(array: number[]): void {
    process.stdout.write("[");
    for (let i = 0; i < array.length; i ++) {
        if (i != 0) {
            process.stdout.write(" ");
        }
        process.stdout.write(""+array[i]);
        if (i != array.length - 1) {
            process.stdout.write(",");
        }
    }
    process.stdout.write("]\n");
    return;
}

function sorted(array: number[]): boolean {
    var sorted = true;
    for (let i = 0; i < array.length - 1; i ++) {
        sorted = sorted && array[i] <= array[i + 1]
    }
    return sorted;
}

function shuffle(array: number[]): void {
    var index1 = Math.floor(Math.random() * array.length);
    var index2 = Math.floor(Math.random() * array.length);
    if (index1 == index2) {
        shuffle(array);
    } else {
        var temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
    return;
}

function bogoSort(array: number[]): void {
    while (!sorted(array)) {
        shuffle(array);
    }
    return;
}

function insertionSort(array: number[]): void {
    for (let i = 1; i < array.length; i ++) {
        var temp = array[i];
        var j;
        for (j = i - 1; j >= 0 && array[j] > temp; j --) {
            array[j + 1] = array[j];
        }
        array[j + 1] = temp;
    }
    return;
}

function selectionSort(array: number[]): void {
    for (let i = 0; i < array.length; i ++) {
        var mini = i;
        for (let j = i; j < array.length; j ++) {
            if (array[j] < array[mini]) {
                mini = j;
            }
        }
        var temp = array[i];
        array[i] = array[mini];
        array[mini] = temp;
    }
    return;
}

function getMax(array: number[]): number {
    var max = array[0];
    for (let i = 0; i < array.length; i ++) {
        if (array[i] > max) {
            max = array[i];
        }
    }
    return max;
}

function countSort(array: number[], degree: number): void {
    var output: number[] = new Array(array.length);
    var count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    for (let i = 0; i < array.length; i ++) {
        count[Math.floor(array[i] / degree) % 10]++;
    }
    for (let i = 1; i < 10; i ++) {
        count[i] += count[i - 1];
    }
    for (let i = array.length - 1; i >= 0; i --) {
        output[count[Math.floor(array[i] / degree) % 10] - 1] = array[i];
        count[Math.floor(array[i] / degree) % 10]--;
    }
    for (let i = 0; i < array.length; i ++) {
        array[i] = output[i];
    }
    return;
}

function radixSort(array: number[]) {
    var max = getMax(array);
    var i = 1;
    while (Math.floor(max / i) > 0) {
        countSort(array, i);
        i = i * 10;
    }
}

main();
