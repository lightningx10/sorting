#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

typedef struct Sorter {
  void (*func)(int *, int);
  char *name;
} sorter_t;

void benchmark(sorter_t, int, int, FILE *);
void bogo_sort(int *, int);
void count_sort(int *, int, int);
void demo(sorter_t, int);
int *gen_rand_arr(int);
int get_max(int *, int);
void init_csv(FILE *);
void insertion_sort(int *, int);
void print_array(int *, int);
void radix_sort(int *, int);
void selection_sort(int *, int);
void shuffle(int *, int);
int sorted(int *, int);

int main() {
  printf("Starting sorting demonstrations in C\n");
  sorter_t insertion, selection, radix, bogo;
  insertion = (sorter_t){insertion_sort, "Insertion"};
  selection = (sorter_t){selection_sort, "Selection"};
  radix = (sorter_t){radix_sort, "Radix"};
  bogo = (sorter_t){bogo_sort, "Bogo"};
  demo(insertion, 10);
  demo(selection, 10);
  demo(radix, 10);
  demo(bogo, 11);
  demo(bogo, 12);

  printf("Starting timed sorting in C\n");
  FILE *fp;
  if ((fp = fopen("results/c.csv", "w+")) != NULL) {
    init_csv(fp);
    benchmark(insertion, 1200, 10000, fp);
    benchmark(insertion, 5000, 10000, fp);
    benchmark(insertion, 12000, 10000, fp);
    benchmark(selection, 1200, 10000, fp);
    benchmark(selection, 5000, 10000, fp);
    benchmark(selection, 12000, 10000, fp);
    benchmark(radix, 1200, 10000, fp);
    benchmark(radix, 5000, 10000, fp);
    benchmark(radix, 12000, 10000, fp);
    benchmark(bogo, 11, 1, fp);
    benchmark(bogo, 12, 1, fp);
    benchmark(bogo, 13, 1, fp);
  } else {
    fprintf(stderr, "Failed to open/make results file\n");
    return 1;
  }
  if (fclose(fp)) {
    fprintf(stderr, "Failed to close results file\n");
    return 1;
  }

  printf("Done!\n");
  return 0;
}

void demo(sorter_t sort, int arr_length) {
  int *arr = gen_rand_arr(arr_length);
  printf("%s sorting demonstration on an array of size %d:\n", sort.name,
         arr_length);
  printf("Before:\n");
  print_array(arr, arr_length);
  sort.func(arr, arr_length);
  printf("After:\n");
  print_array(arr, arr_length);
  free(arr);
  return;
}

void init_csv(FILE *fp) {
  if (fp != NULL) {
    fprintf(fp, "name,rep,size,time\n");
  }
  return;
}

void benchmark(sorter_t sort, int arr_length, int num_times, FILE *fp) {
  int *arrs[num_times];
  struct timeval start, end;
  double t;

  void (*f)(int *, int) = sort.func;

  for (int i = 0; i < num_times; i++) {
    arrs[i] = gen_rand_arr(arr_length);
  }
  gettimeofday(&start, NULL);
  for (int i = 0; i < num_times; i++) {
    f(arrs[i], arr_length);
  }
  gettimeofday(&end, NULL);
  for (int i = 0; i < num_times; i++) {
    free(arrs[i]);
  }
  t = ((double)(end.tv_sec * 1000000 + end.tv_usec -
                (start.tv_sec * 1000000 + start.tv_usec))) /
      1000000;
  printf("It took %f seconds to do %s sort %d times on arrays of size %d\n", t,
         sort.name, num_times, arr_length);

  if (fp != NULL) {
    fprintf(fp, "%s,%d,%d,%f\n", sort.name, num_times, arr_length, t);
  }
}

void print_array(int *arr, int arr_length) {
  printf("[");
  for (int i = 0; i < arr_length; i++) {
    printf("%d", arr[i]);
    if (i != arr_length - 1) {
      printf(", ");
    }
  }
  printf("]\n");
}

int *gen_rand_arr(int arr_length) {
  int *arr;
  if ((arr = malloc(arr_length * sizeof(int))) == NULL) {
    exit(1);
  }
  for (int i = 0; i < arr_length; i++) {
    arr[i] = arc4random_uniform(100);
  }
  return arr;
}

void shuffle(int *arr, int arr_length) {
  int index1, index2;
  index1 = arc4random_uniform(arr_length);
  index2 = arc4random_uniform(arr_length);
  if (index1 == index2) {
    shuffle(arr, arr_length);
  } else {
    int temp = arr[index1];
    arr[index1] = arr[index2];
    arr[index2] = temp;
  }
  return;
}

int sorted(int *arr, int arr_length) {
  int sorted = 1;
  for (int i = 0; i < arr_length - 1; i++) {
    sorted = sorted && arr[i] <= arr[i + 1];
  }
  return sorted;
}

/* Sorting Functions */

/*
** Insertion sort should have a very similar effiency to selection sort, it
** should be slightly faster at best, because it can take best case scenarios.
*/
void insertion_sort(int *arr, int arr_length) {
  for (int i = 1; i < arr_length; i++) {
    int temp = arr[i];
    int j;
    for (j = i - 1; j >= 0 && arr[j] > temp; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = temp;
  }
  return;
}

/*
** Selection sort should be very similar in complexity to insertion sort, in
** this case it will be slightly slower, because the complexity is always
** O(n^2).
*/
void selection_sort(int *arr, int arr_length) {
  for (int i = 0; i < arr_length; i++) {
    int mini = i;
    for (int j = i; j < arr_length; j++) {
      if (arr[j] < arr[mini]) {
        mini = j;
      }
    }
    int temp = arr[i];
    arr[i] = arr[mini];
    arr[mini] = temp;
  }
  return;
}

/*
** Bogo sort is a joke algorithm, it is incredibly inefficient. To do what
** selection and insertion do in one second would take days.
*/
void bogo_sort(int *arr, int arr_length) {
  while (!sorted(arr, arr_length)) {
    shuffle(arr, arr_length);
  }
  return;
}

int get_max(int *arr, int arr_length) {
  int max = arr[0];
  for (int i = 0; i < arr_length; i++) {
    if (arr[i] > max) {
      max = arr[i];
    }
  }
  return max;
}

void count_sort(int *arr, int arr_length, int deg) {
  int output[arr_length];
  int i, count[10] = {0};

  for (i = 0; i < arr_length; i++) {
    count[(arr[i] / deg) % 10]++;
  }

  for (i = 1; i < 10; i++) {
    count[i] += count[i - 1];
  }

  for (i = arr_length - 1; i >= 0; i--) {
    output[count[(arr[i] / deg) % 10] - 1] = arr[i];
    count[(arr[i] / deg) % 10]--;
  }

  for (i = 0; i < arr_length; i++) {
    arr[i] = output[i];
  }
}

void radix_sort(int *arr, int arr_length) {
  int max = get_max(arr, arr_length);
  for (int i = 1; max / i > 0; i *= 10)
    count_sort(arr, arr_length, i);
}
