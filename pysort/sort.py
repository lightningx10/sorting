import time
import numpy as np
import random

def main():
    print("Starting sorting demonstrations in Python")
    demo(insertion_sort, 10, "Insertion")
    demo(selection_sort, 10, "Selection");
    demo(radix_sort, 10, "Radix");
    demo(bogo_sort, 7, "Bogo");
    demo(bogo_sort, 8, "Bogo");

    print("Starting timed sorting in Python")
    try:
        f = open("results/py.csv", "w")
        init_csv(f)
        benchmark(insertion_sort, 1200, 10, "Insertion", f);
        benchmark(insertion_sort, 5000, 2, "Insertion", f);
        benchmark(insertion_sort, 12000, 1, "Insertion", f);
        benchmark(selection_sort, 1200, 10, "Selection", f);
        benchmark(selection_sort, 5000, 2, "Selection", f);
        benchmark(selection_sort, 12000, 1, "Selection", f);
        benchmark(radix_sort, 1200, 10, "Radix", f);
        benchmark(radix_sort, 5000, 2, "Radix", f);
        benchmark(radix_sort, 12000, 1, "Radix", f);
        benchmark(radix_sort, 12000, 10, "Radix", f);
        benchmark(bogo_sort, 7, 1, "Bogo", f);
        benchmark(bogo_sort, 8, 1, "Bogo", f);
        benchmark(bogo_sort, 9, 1, "Bogo", f);
        f.close()
    except Exception as e:
        print(e)
        print("Failed to open/make/write/close results file")

    print("Done!")
    return

def demo(f, size, name):
    array = gen_rand_arr(size)
    print(name+" sorting demonstration on an array of size "+str(size))
    print("Before:")
    print_array(array)
    f(array)
    print("After:")
    print_array(array)
    return

def init_csv(f):
    f.write("name,rep,size,time\n")
    return

def benchmark(f, size, n, name, fo):
    arrays = []
    for i in range(n):
        arrays.append(gen_rand_arr(size))
    start = time.time()
    for i in range(n):
        f(arrays[i])
    end = time.time()
    delta = end - start
    print("It took "+str(delta)+" seconds to do "+name+" sort "+str(n)+
          " times on arrays of size "+str(size))
    fo.write(name+","+str(n)+","+str(size)+","+str(delta)+"\n")
    return

def print_array(array):
    print(array)
    return

def gen_rand_arr(size):
    array = np.zeros(size)
    for i in range(size):
        array[i] = random.randrange(0,100)
    return array

def shuffle(array):
    index1 = random.randrange(0,array.size)
    index2 = random.randrange(0,array.size)
    if index1 == index2:
        shuffle(array)
    else:
        temp = array[index1]
        array[index1] = array[index2]
        array[index2] = temp
    return

def sorted(array):
    sorted = True
    for i in range(array.size - 1):
        sorted = sorted and array[i] <= array[i + 1]
    return sorted

def insertion_sort(array):
    for i in range(1, array.size):
        temp = array[i]
        j = i - 1
        while (j >= 0 and array[j] > temp):
            array[j + 1] = array[j]
            j -= 1
        array[j + 1] = temp
    return

def selection_sort(array):
    for i in range(array.size):
        mini = i
        j = i
        while (j < array.size):
            if (array[j] < array[mini]):
                mini = j
            j += 1
        temp = array[i]
        array[i] = array[mini]
        array[mini] = temp
    return

def bogo_sort(array):
    while (not sorted(array)):
        shuffle(array)
    return

def get_max(array):
    max = array[0]
    for i in range(array.size):
        if (array[i] > max):
            max = array[i]
    return max

def count_sort(array, degree):
    output = np.zeros(array.size)
    count = np.zeros(10)

    for i in range(array.size):
        count[int(array[i] / degree) % 10] += 1

    for i in range(1, 10):
        count[i] += count[i - 1]

    i = array.size - 1
    while (i >= 0):
        output[int(count[int(array[i] / degree) % 10] - 1)] = array[i]
        count[int(array[i] / degree) % 10] -= 1
        i -= 1

    for i in range(array.size):
        array[i] = output[i]
    return

def radix_sort(array):
    max = get_max(array)
    i = 1
    while (int(max / i) > 0):
        count_sort(array, i)
        i *= 10

if __name__ == "__main__":
    main()
