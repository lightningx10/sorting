package main

import (
	"fmt"
	"math"
	"math/rand"
	"os"
	"time"
)

func main() {
	fmt.Println("Starting sorting demonstrations in Go")
	demo(insertionSort, 10, "Insertion")
	demo(selectionSort, 10, "Selection")
	demo(bogoSort, 11, "Bogo")
	demo(bogoSort, 12, "Bogo")

	fmt.Println("Starting timed sorting in Go")
	var file, err = os.OpenFile("results/go.csv", os.O_RDWR|os.O_CREATE, 0755)
	if err == nil {
		initCSV(file)
		benchmark(insertionSort, 1200, 10000, "Insertion", file)
		benchmark(insertionSort, 5000, 10000, "Insertion", file)
		benchmark(insertionSort, 12000, 10000, "Insertion", file)
		benchmark(selectionSort, 1200, 10000, "Selection", file)
		benchmark(selectionSort, 5000, 10000, "Selection", file)
		benchmark(selectionSort, 12000, 10000, "Selection", file)
		benchmark(radixSort, 1200, 10000, "Radix", file)
		benchmark(radixSort, 5000, 10000, "Radix", file)
		benchmark(radixSort, 12000, 10000, "Radix", file)
		benchmark(bogoSort, 11, 1, "Bogo", file)
		benchmark(bogoSort, 12, 1, "Bogo", file)
		benchmark(bogoSort, 13, 1, "Bogo", file)
	} else {
		fmt.Println("Failed to open/make results file")
		return
	}
	if file.Close() != nil {
		fmt.Println("Failed to close results file")
		return
	}

	fmt.Println("Done!")
	return
}

func demo(f func([]int), arrSize int, name string) {
	var arr = genRandArr(arrSize)
	fmt.Println(name + " sorting demonstration on an array of size " +
		fmt.Sprint(arrSize) + ":")
	fmt.Println("Before:")
	fmt.Println(arr)
	f(arr)
	fmt.Println("After:")
	fmt.Println(arr)
}

func initCSV(file *os.File) {
	fmt.Fprintln(file, "name,rep,size,time")
}

func benchmark(f func([]int), arrSize int, numTimes int, name string, file *os.File) {
	var i int
	var arrs = make([][]int, numTimes)
	for i = 0; i < numTimes; i++ {
		arrs[i] = genRandArr(arrSize)
	}
	var start = time.Now().UnixNano()
	for i = 0; i < numTimes; i++ {
		f(arrs[i])
	}
	var end = time.Now().UnixNano()
	var time = float64(end-start) / math.Pow(10, 9)
	fmt.Println("It took " + fmt.Sprint(time) + " seconds to do " + name +
		" sort " + fmt.Sprint(numTimes) + " times on arrays of size " +
		fmt.Sprint(arrSize))
	fmt.Fprintln(file, name+","+fmt.Sprint(numTimes)+","+
		fmt.Sprint(arrSize)+","+fmt.Sprint(time))
}

func genRandArr(arrSize int) (randArr []int) {
	var array = make([]int, arrSize)
	var i int
	rand.Seed(time.Now().UnixNano())
	for i = 0; i < len(array); i++ {
		array[i] = rand.Intn(100)
	}
	return array
}

func sorted(array []int) (result bool) {
	var sorted = true
	var i int
	for i = 0; i < len(array)-1; i++ {
		sorted = sorted && array[i] <= array[i+1]
	}
	return sorted
}

func shuffle(array []int) {
	var index1, index2 int
	index1 = rand.Intn(len(array))
	index2 = rand.Intn(len(array))
	if index1 == index2 {
		shuffle(array)
	} else {
		var temp = array[index1]
		array[index1] = array[index2]
		array[index2] = temp
	}
}

func insertionSort(array []int) {
	var i, j int
	for i = 1; i < len(array); i++ {
		var temp = array[i]
		for j = i - 1; j >= 0 && array[j] > temp; j-- {
			array[j+1] = array[j]
		}
		array[j+1] = temp
	}
}

func selectionSort(array []int) {
	var i, j int
	for i = 0; i < len(array); i++ {
		var mini = i
		for j = i; j < len(array); j++ {
			if array[j] < array[mini] {
				mini = j
			}
		}
		var temp = array[i]
		array[i] = array[mini]
		array[mini] = temp
	}
}

func bogoSort(array []int) {
	for !sorted(array) {
		shuffle(array)
	}
}

func getMax(array []int) (max int) {
	max = array[0]
	for _, x := range array {
		if x > max {
			max = x
		}
	}
	return max
}

func countSort(array []int, deg int) {
	var (
		output = make([]int, len(array))
		i      int
		count  = new([10]int)
	)
	for i = 0; i < len(array); i++ {
		count[(array[i]/deg)%10]++
	}
	for i = 1; i < 10; i++ {
		count[i] += count[i-1]
	}
	for i = len(array) - 1; i >= 0; i-- {
		output[count[(array[i]/deg)%10]-1] = array[i]
		count[(array[i]/deg)%10]--
	}
	for i = 0; i < len(array); i++ {
		array[i] = output[i]
	}
}

func radixSort(array []int) {
	var max = getMax(array)
	for i := 1; max/i > 0; i *= 10 {
		countSort(array, i)
	}
}
