include config.mk

all: $(ALL)
clean:
	rm sortWithC sortWithCPP sortWithCPPVec sortWithGo sortWithJava javasort/*.class sortWithPython sortWithCSharp sortWithRust sortWithJavaScript sortWithTypeScript sortWithHaskell
	rm javasort/results pysort/results csharpsort/results jssort/results tssort/results haskellsort/results
	rm csharpsort/sort.exe csharpsort/sort.exe.so
	cd rustsort && cargo clean
	rm tssort/dist/*
	cd haskellsort && cabal clean

sortWithC: csort/sort.c
	${CC} ${LIBS} ${CFLAGS} -o sortWithC csort/sort.c
	chmod +x ./sortWithC
sortWithCPP: cppsort/sort.cpp cppsort/vectorsort.cpp
	${CPP} ${LIBS} ${CFLAGS} -o sortWithCPP cppsort/sort.cpp
	${CPP} ${LIBS} ${CFLAGS} -o sortWithCPPVec cppsort/vectorsort.cpp
	chmod +x ./sortWithCPP
	chmod +x ./sortWithCPPVec
sortWithGo: gosort/sort.go
	go build -o sortWithGo gosort/sort.go
	chmod +x ./sortWithGo
sortWithJava: javasort/sort.java
	javac javasort/sort.java
	echo -e "#!/bin/sh\ncd javasort\njava -Xmx2g sort" > sortWithJava
	cd javasort && ln -s ../results ./
	chmod +x sortWithJava
sortWithPython: pysort/sort.py
	echo -e "#!/bin/sh\ncd pysort\npython sort.py" > sortWithPython
	cd pysort && ln -s ../results ./
	chmod +x sortWithPython
sortWithCSharp: csharpsort/sort.cs
	cd csharpsort && mcs sort.cs && mono --aot -O=all sort.exe
	echo -e "#!/bin/sh\ncd csharpsort\nmono sort.exe" > sortWithCSharp
	cd csharpsort/ && ln -s ../results ./
	chmod +x sortWithCSharp
sortWithRust: rustsort/src/main.rs rustsort/Cargo.toml
	cd rustsort && cargo build --release
	cp rustsort/target/release/rustsort sortWithRust
	chmod +x sortWithRust
sortWithJavaScript: jssort/src/sort.js
	cd jssort && ln -s ../results ./
	echo -e "#!/bin/sh\ncd jssort\nnpm start" > sortWithJavaScript
	chmod +x sortWithJavaScript
sortWithTypeScript: tssort/src/sort.ts
	cd tssort && ln -s ../results ./
	cd tssort && npx tsc
	echo -e "#!/bin/sh\ncd tssort\nnpm start" > sortWithTypeScript
	chmod +x sortWithTypeScript
sortWithHaskell: haskellsort/app/Main.hs haskellsort/app/Sort.hs
	cd haskellsort && cabal build
	cd haskellsort && ln -s ../results ./
	echo -e "#!/bin/sh\ncd haskellsort\ncabal run" > sortWithHaskell
	chmod +x sortWithHaskell
