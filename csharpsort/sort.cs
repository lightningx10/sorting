using System;
using System.IO;
using System.Text;

namespace csharpsort
{
    class sort
    {
        static void Main(string[] args) {
            System.Console.WriteLine("Starting sorting demonstrations in C#");
            demo(insertionSort, 10, "Insertion");
            demo(selectionSort, 10, "Selection");
            demo(radixSort, 10, "Radix");
            //demo(bogoSort, 11, "Bogo");
            //demo(bogoSort, 12, "Bogo");

            System.Console.WriteLine("Starting timed sorting in C#");
            try {
                StreamWriter sw = new StreamWriter("results/csharp.csv", false, Encoding.UTF8);
                initCSV(sw);
                benchmark(insertionSort, 1200, 10000, "Insertion", sw);
                benchmark(insertionSort, 5000, 10000, "Insertion", sw);
                benchmark(insertionSort, 12000, 10000, "Insertion", sw);
                benchmark(selectionSort, 1200, 10000, "Selection", sw);
                benchmark(selectionSort, 5000, 10000, "Selection", sw);
                benchmark(selectionSort, 12000, 10000, "Selection", sw);
                benchmark(radixSort, 1200, 10000, "Radix", sw);
                benchmark(radixSort, 5000, 10000, "Radix", sw);
                benchmark(radixSort, 12000, 10000, "Radix", sw);
                //benchmark(bogoSort, 11, 1, "Bogo", sw);
                //benchmark(bogoSort, 12, 1, "Bogo", sw);
                //benchmark(bogoSort, 13, 1, "Bogo", sw);
                sw.Close();
            } catch (Exception e) {
                System.Console.WriteLine("Exception: " + e.Message);
                System.Console.WriteLine("Failed to open/close/write results file");
            }
        }

        static void demo(Action<int[]> f, int arrSize, string name) {
            int[] arr = genRandArr(arrSize);
            System.Console.WriteLine(name+" sorting demonstration on an array of size "+arrSize+":");
            System.Console.WriteLine("Before:");
            printArray(arr);
            f(arr);
            System.Console.WriteLine("After:");
            printArray(arr);
        }

        static void initCSV(StreamWriter output) {
            output.WriteLine("name,rep,size,time");
        }

        static void benchmark(Action<int[]> f, int arrSize, int numTimes, string name, StreamWriter output) {
            int[][] arrs = new int[numTimes][];
            var watch = new System.Diagnostics.Stopwatch();
            for (int i = 0; i < numTimes; i++) {
                arrs[i] = genRandArr(arrSize);
            }
            watch.Start();
            for (int i = 0; i < numTimes; i++) {
                f(arrs[i]);
            }
            watch.Stop();
            System.Console.WriteLine("It took "+(double)watch.ElapsedMilliseconds/1000+
                                     " seconds to do "+name+" sort "+numTimes+
                                     " times on arrays of size "+arrSize);
            output.WriteLine(name+","+numTimes+","+arrSize+","+(double)watch.ElapsedMilliseconds/1000);
        }

        static void printArray(int[] arr) {
            System.Console.Write('[');
            for (int i = 0; i < arr.Length; i++) {
                if (i != 0) {
                    System.Console.Write(' ');
                }
                System.Console.Write(arr[i]);
                if (i != arr.Length - 1) {
                    System.Console.Write(',');
                }
            }
            System.Console.WriteLine(']');
        }

        static int[] genRandArr(int arrSize) {
            int[] arr = new int[arrSize];
            Random rnd = new Random();
            for (int i = 0; i < arrSize; i++) {
                arr[i] = rnd.Next(100);
            }
            return arr;
        }

        static void shuffle(int[] arr) {
            int index1, index2;
            Random rnd = new Random();
            index1 = rnd.Next(arr.Length);
            index2 = rnd.Next(arr.Length);
            if (index1 == index2) {
                shuffle(arr);
            } else {
                int temp = arr[index1];
                arr[index1] = arr[index2];
                arr[index2] = temp;
            }
        }

        static bool sorted(int[] arr) {
            bool sorted = true;
            for (int i = 0; i < arr.Length - 1; i++) {
                sorted = sorted && arr[i] <= arr[i+1];
            }
            return sorted;
        }

        static void insertionSort(int[] arr) {
            for (int i = 1; i < arr.Length; i++) {
                int temp = arr[i];
                int j;
                for (j = i - 1; j >= 0 && arr[j] > temp; j--) {
                    arr[j+1] = arr[j];
                }
                arr[j+1] = temp;
            }
        }

        static void selectionSort(int[] arr) {
            for (int i = 0; i < arr.Length; i++) {
                int mini = i;
                for (int j = i; j < arr.Length; j++) {
                    if (arr[j] < arr[mini]) {
                        mini = j;
                    }
                }
                int temp = arr[i];
                arr[i] = arr[mini];
                arr[mini] = temp;
            }
        }

        static void bogoSort(int[] arr) {
            while (!sorted(arr)) {
                shuffle(arr);
            }
        }

        static int getMax(int[] arr) {
            int max = arr[0];
            for (int i = 0; i < arr.Length; i++) {
                if (arr[i] > max) {
                    max = arr[i];
                }
            }
            return max;
        }

        static void countSort(int[] arr, int deg) {
            int[] output = new int[arr.Length];
            int[] count = new int[10];
            int i;

            for (i = 0; i < arr.Length; i++) {
                count[(arr[i] / deg) % 10]++;
            }

            for (i = 1; i < 10; i++) {
                count[i] += count[i - 1];
            }

            for (i = arr.Length - 1; i >=0; i--) {
                output[count[(arr[i] / deg) % 10] - 1] = arr[i];
                count[(arr[i] / deg) % 10]--;
            }

            for (i = 0; i < arr.Length; i++) {
                arr[i] = output[i];
            }
        }

        static void radixSort(int[] arr) {
            int max = getMax(arr);
            for (int i = 1; max / i > 0; i *= 10) {
                countSort(arr, i);
            }
        }
    }
}
