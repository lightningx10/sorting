implement Sort;

include "daytime.m";
include "draw.m";
include "sys.m";
include "rand.m";
include "arg.m";

Sort: module {
    init: fn(nil: ref Draw->Context, argv: list of string);
};

init(nil: ref Draw->Context, argv: list of string) {
    sys := load Sys Sys->PATH;
    sys->print("Starting sorting demonstrations in Limbo\n");
    demo(insertion_sort, 10, "Insertion");
    demo(selection_sort, 10, "Selection");
    #demo(radix_sort, 10, "Radix");
    #demo(bogo_sort, 3, "Bogo");
    #demo(bogo_sort, 12, "Bogo");

    sys->print("Starting timed sorting in Limbo\n");
    benchmark(insertion_sort, 1200, 10000, "Insertion");
    benchmark(selection_sort, 1200, 10000, "Selection");
    #benchmark(radix_sort, 1200, 10000, "Radix");
    #benchmark(bogo_sort, 11, 1, "Bogo");

    sys->print("Done!\n");
    exit;
}

demo(f: ref fn(arr: array of int, s: int): array of int, size: int, name: string) {
    sys := load Sys Sys->PATH;
    arr := gen_rand_array(size);
    sys->print("%s sorting demonstration on an array of size %d:\n", name, size);
    sys->print("Before:\n");
    print_array(arr, size);
    f(arr, size);
    sys->print("After:\n");
    print_array(arr, size);
    return;
}

benchmark(f: ref fn(l: array of int, s: int): array of int, size: int, n: int, name: string) {
    sys := load Sys Sys->PATH;
    start := sys->millisec();
    for (i := 0; i < size; i++) {
        f(gen_rand_array(size), size);
    }
    end := sys->millisec();
    t := real (end - start) / 1000.00;
    sys->print("It took %f seconds to do %s sort %d times on arrays of size %d\n", t, name, n, size);
    return;
}

print_array(arr: array of int, size: int) {
    sys := load Sys Sys->PATH;
    sys->print("[");
    for (i := 0; i < size; i++) {
        sys->print("%d", arr[i]);
        if (i != size - 1) {
            sys->print(", ");
        }
    }
    sys->print("]\n");
}

gen_rand_array(size: int): array of int {
    sys := load Sys Sys->PATH;
    rand := load Rand Rand->PATH;
    rand->init(sys->millisec());
    arr := array[size] of int;
    for (i := 0; i < size; i++) {
        arr[i] = rand->rand(100);
    }
    return arr;
}

shuffle(arr: array of int, size: int): array of int {
    rand := load Rand Rand->PATH;
    index1 := rand->rand(size);
    index2 := rand->rand(size);
    if (index1 == index2) {
        return shuffle(arr, size);
    } else {
        temp := arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = temp;
    }
    return arr;
}

sorted(arr: array of int, size: int): int {
    sorted: int = 1;
    for (i := 0; i < size - 1; i++) {
        sorted = sorted && arr[i] <= arr[i + 1];
    }
    return sorted;
}

insertion_sort(arr: array of int, size: int): array of int {
    for (i := 1; i < size; i++) {
        temp := arr[i];
        j: int;
        for (j = i - 1; j >= 0 && arr[j] > temp; j--) {
            arr[j + 1] = arr[j];
        }
        arr[j + 1] = temp;
    }
    return arr;
}

selection_sort(arr: array of int, size: int): array of int {
    for (i := 0; i < size; i++) {
        mini := i;
        for (j := i; j < size; j++) {
            if (arr[j] < arr[mini]) {
                mini = j;
            }
        }
        temp := arr[i];
        arr[i] = arr[mini];
        arr[mini] = temp;
    }
    return arr;
}

bogo_sort(arr: array of int, size: int): array of int {
    narr := arr;
    while (!sorted(narr, size)) {
        narr = shuffle(narr, size);
    }
    return narr;
}
