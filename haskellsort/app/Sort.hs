module Sort
  ( -- * Sort functions
    randList,
    selectionSort,
    insertionSort,
    bogoSort,
    radixSort,
    mergeSort,
  )
where

import qualified Control.Monad
import Data.List (delete, permutations)
import System.Random (randomRIO)

-- None of these functions are my own, they were all taken from
-- https://devtut.github.io/haskell/sorting-algorithms.html#insertion-sort

randList :: Int -> IO [Integer]
randList n = Control.Monad.replicateM n (randomRIO (0, 100 :: Integer))

selectionSort :: Ord a => [a] -> [a]
selectionSort [] = []
selectionSort xs =
  let x = minimum xs
   in x : selectionSort (delete x xs)

insertionSort :: Ord a => [a] -> [a]
insertionSort [] = []
insertionSort (x : xs) = insert x (insertionSort xs)
  where
    insert :: Ord a => a -> [a] -> [a]
    insert a [] = [a]
    insert a (b : bs)
      | a < b = a : b : bs
      | otherwise = b : insert a bs

sorted :: Ord a => [a] -> Bool
sorted (x : y : ys) = x <= y && sorted (y : ys)
sorted _ = True

bogoSort :: Ord a => [a] -> [a]
bogoSort = head . filter sorted . permutations

radixSort :: Ord a => [a] -> [a]
radixSort = undefined

mergeSort :: Ord a => [a] -> [a]
mergeSort [] = []
mergeSort [a] = [a]
mergeSort xs = merge (mergeSort (firstHalf xs)) (mergeSort (secondHalf xs))
  where
    firstHalf :: Ord a => [a] -> [a]
    firstHalf ys = let n = length ys in take (div n 2) ys
    secondHalf :: Ord a => [a] -> [a]
    secondHalf ys = let n = length ys in drop (div n 2) ys
    merge :: Ord a => [a] -> [a] -> [a]
    merge ys [] = ys
    merge [] zs = zs
    merge (y : ys) (z : zs)
      | y <= z = y : merge ys (z : zs)
      | otherwise = z : merge (y : ys) zs
