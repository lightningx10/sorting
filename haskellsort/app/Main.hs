module Main
  ( -- * Main functions
    main,
  )
where

import Control.DeepSeq (deepseq)
import Sort (bogoSort, insertionSort, mergeSort, randList, selectionSort)
import System.CPUTime (getCPUTime)
import System.IO
import Text.Printf (printf)

main :: IO ()
main = do
  printf "Starting sorting demonstrations in Haskell\n"
  demo insertionSort 10 "Insertion"
  demo selectionSort 10 "Selection"
  demo mergeSort 10 "Merge"
  demo bogoSort 11 "Bogo"
  demo bogoSort 12 "Bogo"

  printf "Starting timed sorting in Haskell\n"
  resFile <- openFile "results/haskell.csv" WriteMode
  initCSV resFile
  benchmark insertionSort 1200 10000 "Insertion" resFile
  -- benchmark insertionSort 5000 10000 "Insertion" resFile
  -- benchmark insertionSort 12000 10000 "Insertion" resFile
  benchmark selectionSort 1200 10000 "Selection" resFile
  -- benchmark selectionSort 5000 10000 "Selection" resFile
  -- benchmark selectionSort 12000 10000 "Selection" resFile
  benchmark mergeSort 1200 10000 "Merge" resFile
  benchmark mergeSort 5000 10000 "Merge" resFile
  benchmark mergeSort 12000 10000 "Merge" resFile
  benchmark bogoSort 11 1 "Bogo" resFile
  benchmark bogoSort 12 1 "Bogo" resFile
  benchmark bogoSort 13 1 "Bogo" resFile
  hClose resFile

  printf "Done!\n"

demo :: ([Integer] -> [Integer]) -> Int -> String -> IO ()
demo f size name = do
  printf (name ++ " sorting demonstration on an array of size " ++ show size ++ "\n")
  arr <- randList size
  printf "Before:\n"
  print arr
  printf "After:\n"
  printf (show (f arr) ++ "\n")

initCSV :: Handle -> IO ()
initCSV csvFile = do
  hPutStrLn csvFile "name,rep,size,time"

benchmark :: ([Integer] -> [Integer]) -> Int -> Int -> String -> Handle -> IO ()
benchmark f size n name resFile = do
  start <- getCPUTime
  result <- helper n
  end <- result `deepseq` getCPUTime
  let diff = fromIntegral (end - start) / (10.0 ** 12.0)
  printf "It took %f seconds to do %s sort %d times on arrays of size %d\n" (diff :: Double) (name :: String) (n :: Int) (size :: Int)
  hPutStrLn resFile (name ++ "," ++ show n ++ "," ++ show size ++ "," ++ show diff)
  return ()
  where
    helper :: Int -> IO ()
    helper 0 = return ()
    helper i = do
      arr <- randList size
      let result = f arr
      result `deepseq` helper (i - 1)
