CC = egcc
CPP = clang++
CFLAGS = -pedantic -Wall -Ofast
ALL = sortWithC \
      sortWithCPP \
      sortWithGo \
      sortWithJava \
      sortWithPython \
      sortWithCSharp \
      sortWithRust \
      sortWithJavaScript \
      sortWithTypeScript \
      sortWithHaskell

# Linux: (uncomment the following lines)
#CC = gcc
#CPP = g++
#LIBS = -lbsd -lm
