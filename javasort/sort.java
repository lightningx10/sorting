import java.io.*;
import java.util.Random;

public class sort {
  public interface SortingFunction {
    void sort(int[] array);
  }

  public static int[] genRandArray(int arraySize) {
    int[] ret = new int[arraySize];
    Random rand = new Random();
    for (int i = 0; i < ret.length; i++) {
      ret[i] = rand.nextInt(100);
    }
    return ret;
  }

  public static void insertionSort(int[] array) {
    for (int i = 1; i < array.length; i++) {
      int temp = array[i];
      int j;
      for (j = i - 1; j >= 0 && array[j] > temp; j--) {
        array[j + 1] = array[j];
      }
      array[j + 1] = temp;
    }
  }

  public static void selectionSort(int[] array) {
    for (int i = 0; i < array.length; i++) {
      int mini = i;
      for (int j = i; j < array.length; j++) {
        if (array[j] < array[mini]) {
          mini = j;
        }
      }
      int temp = array[i];
      array[i] = array[mini];
      array[mini] = temp;
    }
  }

  public static boolean sorted(int[] array) {
    boolean ret = true;
    for (int i = 0; i < array.length - 1; i++) {
      ret = ret && array[i] <= array[i + 1];
    }
    return ret;
  }

  public static void shuffle(int[] array) {
    Random rand = new Random();
    int index1, index2;
    index1 = rand.nextInt(array.length);
    index2 = rand.nextInt(array.length);
    if (index1 == index2) {
      shuffle(array);
    } else {
      int temp = array[index1];
      array[index1] = array[index2];
      array[index2] = temp;
    }
  }

  public static void bogoSort(int[] array) {
    while (!sorted(array)) {
      shuffle(array);
    }
  }

  public static int getMax(int[] array) {
    int max = array[0];
    for (int i : array) {
      if (i > max) {
        max = i;
      }
    }
    return max;
  }

  public static void countSort(int[] array, int deg) {
    int[] output = new int[array.length];
    int[] count = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    int i;

    for (i = 0; i < array.length; i++) {
      count[(array[i] / deg) % 10]++;
    }
    for (i = 1; i < 10; i++) {
      count[i] += count[i - 1];
    }
    for (i = array.length - 1; i >= 0; i--) {
      output[count[(array[i] / deg) % 10] - 1] = array[i];
      count[(array[i] / deg) % 10]--;
    }
    for (i = 0; i < array.length; i++) {
      array[i] = output[i];
    }
  }

  public static void radixSort(int[] array) {
    int max = getMax(array);
    for (int i = 1; max / i > 0; i *= 10) {
      countSort(array, i);
    }
  }

  public static void demo(SortingFunction func, int arraySize, String name) {
    int[] array = genRandArray(arraySize);
    System.out.println(name + " sorting demonstration on an array of size " + arraySize + ":");
    System.out.println("Before:");
    printArray(array);
    func.sort(array);
    System.out.println("After:");
    printArray(array);
  }

  public static void initCSV(BufferedOutputStream bo) throws IOException {
    String out = "name,rep,size,time";
    for (char c : out.toCharArray()) {
      bo.write(c);
    }
    bo.write('\n');
  }

  public static void benchmark(SortingFunction func, int arraySize, int numTimes, String name, BufferedOutputStream bo)
      throws IOException {
    int[][] arrs = new int[numTimes][arraySize];
    for (int i = 0; i < numTimes; i++) {
      arrs[i] = genRandArray(arraySize);
    }
    long start = System.nanoTime();
    for (int i = 0; i < numTimes; i++) {
      func.sort(arrs[i]);
    }
    long end = System.nanoTime();
    double time = (double) (end - start) / 1e9;
    System.out.println(
        "It took " + time + " seconds to do " + name + " sort " + numTimes + " times on arrays of size " + arraySize);
    String out = name + "," + numTimes + "," + arraySize + "," + time;
    for (char c : out.toCharArray()) {
      bo.write(c);
    }
    bo.write('\n');
  }

  public static void printArray(int[] array) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append('[');
    for (int i = 0; i < array.length; i++) {
      stringBuilder.append(array[i]);
      if (i != array.length - 1) {
        stringBuilder.append(',');
      }
    }
    stringBuilder.append(']');
    System.out.println(stringBuilder);
  }

  public static void main(String[] args) {
    SortingFunction insertion = (array -> {
      insertionSort(array);
    });
    SortingFunction selection = (array -> {
      selectionSort(array);
    });
    SortingFunction radix = (array -> {
      radixSort(array);
    });
    SortingFunction bogo = (array -> {
      bogoSort(array);
    });

    System.out.println("Starting sorting demonstrations in Java");
    demo(insertion, 10, "Insertion");
    demo(selection, 10, "Selection");
    demo(radix, 10, "Radix");
    demo(bogo, 11, "Bogo");
    demo(bogo, 12, "Bogo");

    System.out.println("Starting timed sorting in Java");
    BufferedOutputStream outputStream;
    try {
      outputStream = new BufferedOutputStream(new FileOutputStream("results/java.csv"));
      initCSV(outputStream);
      benchmark(insertion, 1200, 10000, "Insertion", outputStream);
      benchmark(insertion, 5000, 10000, "Insertion", outputStream);
      benchmark(insertion, 12000, 10000, "Insertion", outputStream);
      benchmark(selection, 1200, 10000, "Selection", outputStream);
      benchmark(selection, 5000, 10000, "Selection", outputStream);
      benchmark(selection, 12000, 10000, "Selection", outputStream);
      benchmark(radix, 1200, 10000, "Radix", outputStream);
      benchmark(radix, 5000, 10000, "Radix", outputStream);
      benchmark(radix, 12000, 10000, "Radix", outputStream);
      benchmark(bogo, 11, 1, "Bogo", outputStream);
      benchmark(bogo, 12, 1, "Bogo", outputStream);
      benchmark(bogo, 13, 1, "Bogo", outputStream);
      outputStream.flush();
      outputStream.close();
    } catch (FileNotFoundException fnf) {
      fnf.printStackTrace();
      System.out.println("Failed to open/make results file");
      return;
    } catch (IOException io) {
      io.printStackTrace();
      System.out.println("Failed to write to results file");
    }

    System.out.println("Done!");
  }
}
