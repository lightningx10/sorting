function main() {
    console.log("Starting sorting demonstrations in JavaScript");
    demo(insertionSort, 10, "Insertion");
    demo(selectionSort, 10, "Selection");
    demo(radixSort, 10, "Radix");
    demo(bogoSort, 12, "Bogo");

    const fs = require('fs');
    const loc = "results/js.csv";
    const fileTup = [fs, loc];
    console.log("Starting timed sorting in JavaScript");
    initCSV(fileTup);
    benchmark(insertionSort, 1200, 10000, "Insertion", fileTup);
    benchmark(insertionSort, 5000, 10000, "Insertion", fileTup);
    benchmark(insertionSort, 12000, 10000, "Insertion", fileTup);
    benchmark(selectionSort, 1200, 10000, "Selection", fileTup);
    benchmark(selectionSort, 5000, 10000, "Selection", fileTup);
    benchmark(selectionSort, 12000, 10000, "Selection", fileTup);
    benchmark(radixSort, 1200, 10000, "Radix", fileTup);
    benchmark(radixSort, 5000, 10000, "Radix", fileTup);
    benchmark(radixSort, 12000, 10000, "Radix", fileTup);
    benchmark(bogoSort, 11, 1, "Bogo", fileTup);
    benchmark(bogoSort, 12, 1, "Bogo", fileTup);
    benchmark(bogoSort, 13, 1, "Bogo", fileTup);

    console.log("Done!");
}

function demo(f, size, name) {
    var array = genRandArray(size);
    console.log(name+" sorting demonstration on an array of size "+size+":");
    console.log("Before:");
    printArray(array);
    f(array);
    console.log("After:");
    printArray(array);
}

function initCSV(fileTup) {
    const fs = fileTup[0];
    const loc = fileTup[1];
    try {
        fs.writeFileSync(loc, "name,rep,size,time\n");
    } catch (err) {
        console.log("Failed to open/make results file");
        console.error(err);
    }
}

function benchmark(f, size, n, name, fileTup) {
    var arrs = new Array(n);
    for (let i = 0; i < n; i ++) {
        arrs[i] = genRandArray(size);
    }
    var start = new Date().getTime();
    for (let i = 0; i < n; i ++) {
        f(arrs[i]);
    }
    var end = new Date().getTime();
    var time = (end - start)/1000;
    console.log("It took " + time + " seconds to do " + name + " sort " + n +
                " times on arrays of size " + size);
    const fs = fileTup[0];
    const loc = fileTup[1];
    try {
        fs.appendFileSync(loc, name + "," + n + "," + size + "," + time + "\n");
    } catch (err) {
        console.log("Failed to append to results file");
        console.error(err);
    }
}

function genRandArray(size) {
    var ret = new Array(size);
    for (let i = 0; i < size; i ++) {
        ret[i] = Math.floor(Math.random() * 100);
    }
    return ret;
}

function printArray(array) {
    process.stdout.write("[");
    for (let i = 0; i < array.length; i++) {
        if (i != 0) {
            process.stdout.write(" ");
        }
        process.stdout.write(""+array[i]);
        if (i != array.length - 1) {
            process.stdout.write(",");
        }
    }
    process.stdout.write("]\n");
}

function shuffle(array) {
    var index1 = Math.floor(Math.random() * array.length);
    var index2 = Math.floor(Math.random() * array.length);
    if (index1 == index2) {
        shuffle(array);
    } else {
        var temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}

function sorted(array) {
    var sorted = true;
    for (let i = 0; i < array.length - 1; i ++) {
        sorted = sorted && array[i] <= array[i + 1];
    }
    return sorted;
}

function bogoSort(array) {
    while (!sorted(array)) {
        shuffle(array);
    }
}

function insertionSort(array) {
    for (let i = 1; i < array.length; i ++) {
        var temp = array[i];
        var j;
        for (j = i - 1; j >= 0 && array[j] > temp; j --) {
            array[j + 1] = array[j];
        }
        array[j + 1] = temp;
    }
}

function selectionSort(array) {
    for (let i = 0; i < array.length; i ++) {
        var mini = i;
        for (let j = i; j < array.length; j ++) {
            if (array[j] < array[mini]) {
                mini = j;
            }
        }
        var temp = array[i];
        array[i] = array[mini];
        array[mini] = temp;
    }
}

function getMax(array) {
    var max = array[0];
    for (let i = 0; i < array.length; i ++) {
        if (array[i] > max) {
            max = array[i];
        }
    }
    return max;
}

function countSort(array, degree) {
    var output = new Array(array.length);
    var count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    for (let i = 0; i < array.length; i++) {
        count[Math.floor(array[i] / degree) % 10]++;
    }
    for (let i = 1; i < 10; i ++) {
        count[i] += count[i - 1];
    }
    for (let i = array.length - 1; i >= 0; i --) {
        output[count[Math.floor(array[i] / degree) % 10] - 1] = array[i];
        count[Math.floor(array[i] / degree) % 10]--;
    }
    for (let i = 0; i < array.length; i ++) {
        array[i] = output[i];
    }
}

function radixSort(array) {
    var max = getMax(array);
    var i = 1;
    while (Math.floor(max / i) > 0) {
        countSort(array, i);
        i = i * 10;
    }
}

// Boiler plate to get a main function.
main();
