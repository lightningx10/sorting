#include <chrono>
#include <fstream>
#include <iostream>

#ifdef linux
#include <algorithm>
#include <bsd/stdlib.h>
#endif

struct Sorter {
  Sorter(void (*f)(int *, int), std::string n) {
    func = f;
    name = n;
  }
  void (*func)(int *, int);
  std::string name;
};

void benchmark(Sorter *, int, int, std::ofstream *);
void bogo_sort(int *, int);
void count_sort(int *, int, int);
void demo(Sorter *, int);
int *gen_rand_arr(int);
int get_max(int *, int);
void insertion_sort(int *, int);
void print_array(int *, int);
void radix_sort(int *, int);
void selection_sort(int *, int);
void shuffle(int *, int);
bool sorted(int *, int);

int main(int argc, char **argv) {
  std::cout << "Starting sorting demonstrations in C++\n";
  Sorter *insertion = new Sorter(insertion_sort, "Insertion");
  Sorter *selection = new Sorter(selection_sort, "Selection");
  Sorter *radix = new Sorter(radix_sort, "Radix");
  Sorter *bogo = new Sorter(bogo_sort, "Bogo");

  demo(insertion, 10);
  demo(selection, 10);
  demo(radix, 10);
  demo(bogo, 11);
  demo(bogo, 12);

  std::cout << "Starting timed sorting in C++\n";
  std::ofstream out;
  out.open("results/cpp.csv");
  if (out.good()) {
    benchmark(insertion, 1200, 10000, &out);
    benchmark(insertion, 5000, 10000, &out);
    benchmark(insertion, 12000, 10000, &out);
    benchmark(selection, 1200, 10000, &out);
    benchmark(selection, 5000, 10000, &out);
    benchmark(selection, 12000, 10000, &out);
    benchmark(radix, 1200, 10000, &out);
    benchmark(radix, 5000, 10000, &out);
    benchmark(radix, 12000, 10000, &out);
    benchmark(bogo, 11, 1, &out);
    benchmark(bogo, 12, 1, &out);
    benchmark(bogo, 13, 1, &out);
  } else {
    std::cerr << "Failed to open/make results file\n";
    return 1;
  }
  out.close();
  if (out.fail()) {
    std::cerr << "Failed to close results file\n";
    return 1;
  }

  delete insertion;
  delete selection;
  delete radix;
  delete bogo;

  std::cout << "Done!\n";
  return 0;
}

void demo(Sorter *sort, int arr_size) {
  int *arr = gen_rand_arr(arr_size);
  std::cout << sort->name << " sorting demonstration on an array of size "
            << arr_size << std::endl;
  std::cout << "Before:\n";
  print_array(arr, arr_size);
  sort->func(arr, arr_size);
  std::cout << "After:\n";
  print_array(arr, arr_size);
  delete[] arr;
  return;
}

void init_csv(std::ofstream *out) {
  if (out != NULL) {
    *out << "name,rep,size,time\n";
  }
  return;
}

void benchmark(Sorter *sort, int arr_size, int num_times, std::ofstream *out) {
  using namespace std::chrono;
  int **arrs = new int *[num_times];

  void (*f)(int *, int) = sort->func;

  for (int i = 0; i < num_times; i++) {
    arrs[i] = gen_rand_arr(arr_size);
  }
  auto start = high_resolution_clock::now();
  for (int i = 0; i < num_times; i++) {
    f(arrs[i], arr_size);
  }
  auto end = high_resolution_clock::now();
  for (int i = 0; i < num_times; i++) {
    delete[] arrs[i];
  }
  auto duration = duration_cast<milliseconds>(end - start);
  std::cout << "It took " << duration.count() / 1000.00 << " seconds to do "
            << sort->name << " sort " << num_times
            << " times on arrays of size " << arr_size << std::endl;
  if (out != NULL) {
    *out << sort->name << ',' << num_times << ',' << arr_size << ','
         << duration.count() / 1000.00 << std::endl;
  }
  delete[] arrs;
  return;
}

void print_array(int *arr, int arr_size) {
  std::cout << '[';
  for (int i = 0; i < arr_size; i++) {
    std::cout << arr[i];
    if (i != arr_size - 1) {
      std::cout << ", ";
    }
  }
  std::cout << "]\n";
  return;
}

int *gen_rand_arr(int arr_size) {
  int *ret = new int[arr_size];
  for (int i = 0; i < arr_size; i++) {
    ret[i] = arc4random_uniform(100);
  }
  return ret;
}

void shuffle(int *arr, int arr_size) {
  int index1, index2;
  index1 = arc4random_uniform(arr_size);
  index2 = arc4random_uniform(arr_size);
  if (index1 == index2) {
    return shuffle(arr, arr_size);
  } else {
    int temp = arr[index1];
    arr[index1] = arr[index2];
    arr[index2] = temp;
  }
  return;
}

bool sorted(int *arr, int arr_size) {
  bool sorted = true;
  for (int i = 0; i < arr_size - 1; i++) {
    sorted = sorted && arr[i] <= arr[i + 1];
  }
  return sorted;
}

void insertion_sort(int *arr, int arr_size) {
  for (int i = 1; i < arr_size; i++) {
    int temp = arr[i];
    int j;
    for (j = i - 1; j >= 0 && arr[j] > temp; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = temp;
  }
  return;
}

void selection_sort(int *arr, int arr_size) {
  for (int i = 0; i < arr_size; i++) {
    int mini = i;
    for (int j = i; j < arr_size; j++) {
      if (arr[j] < arr[mini]) {
        mini = j;
      }
    }
    int temp = arr[i];
    arr[i] = arr[mini];
    arr[mini] = temp;
  }
  return;
}

void bogo_sort(int *arr, int arr_size) {
  while (!sorted(arr, arr_size)) {
    shuffle(arr, arr_size);
  }
  return;
}

int get_max(int *arr, int arr_size) {
  int max = arr[0];
  for (int i = 0; i < arr_size; i++) {
    if (arr[i] > max) {
      max = arr[i];
    }
  }
  return max;
}

void count_sort(int *arr, int arr_size, int deg) {
  int *output = new int[arr_size];
  int i, count[10] = {0};

  for (i = 0; i < arr_size; i++) {
    count[(arr[i] / deg) % 10]++;
  }

  for (i = 1; i < 10; i++) {
    count[i] += count[i - 1];
  }

  for (i = arr_size - 1; i >= 0; i--) {
    output[count[(arr[i] / deg) % 10] - 1] = arr[i];
    count[(arr[i] / deg) % 10]--;
  }

  for (i = 0; i < arr_size; i++) {
    arr[i] = output[i];
  }
  delete[] output;
  return;
}

void radix_sort(int *arr, int arr_size) {
  int max = get_max(arr, arr_size);
  for (int i = 1; max / i > 0; i *= 10) {
    count_sort(arr, arr_size, i);
  }
  return;
}
