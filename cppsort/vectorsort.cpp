#include <chrono>
#include <fstream>
#include <iostream>
#include <vector>

#ifdef linux
#include <algorithm>
#include <bsd/stdlib.h>
#endif

struct Sorter {
  Sorter(void (*f)(std::vector<int> *), std::string n) {
    func = f;
    name = n;
  }
  void (*func)(std::vector<int> *);
  std::string name;
};

void benchmark(Sorter *, int, int, std::ofstream *);
void bogo_sort(std::vector<int> *);
void count_sort(std::vector<int> *, int);
void demo(Sorter *, int);
std::vector<int> *gen_rand_vec(int);
void init_csv(std::ofstream);
void insertion_sort(std::vector<int> *);
void radix_sort(std::vector<int> *);
void selection_sort(std::vector<int> *);
void shuffle(std::vector<int> *);
bool sorted(std::vector<int>);

std::ostream &operator<<(std::ostream &OS, std::vector<int> &vec) {
  OS << '[';
  for (size_t i = 0; i < vec.size(); i++) {
    OS << vec[i];
    if (i != vec.size() - 1) {
      OS << ", ";
    }
  }
  OS << "]\n";
  return OS;
}

int main(int argc, char **argv) {
  std::cout << "Starting sorting demonstrations in C++ with vectors\n";
  Sorter *insertion = new Sorter(insertion_sort, "Insertion");
  Sorter *selection = new Sorter(selection_sort, "Selection");
  Sorter *radix = new Sorter(radix_sort, "Radix");
  Sorter *bogo = new Sorter(bogo_sort, "Bogo");

  demo(insertion, 10);
  demo(selection, 10);
  demo(radix, 10);
  demo(bogo, 11);
  demo(bogo, 12);

  std::cout << "Starting timed sorting in C++ with vectors\n";
  std::ofstream out;
  out.open("results/cppvec.csv");
  if (out.good()) {
    benchmark(insertion, 1200, 10000, &out);
    benchmark(insertion, 5000, 10000, &out);
    benchmark(insertion, 12000, 10000, &out);
    benchmark(selection, 1200, 10000, &out);
    benchmark(selection, 5000, 10000, &out);
    benchmark(selection, 12000, 10000, &out);
    benchmark(radix, 1200, 10000, &out);
    benchmark(radix, 5000, 10000, &out);
    benchmark(radix, 12000, 10000, &out);
    benchmark(bogo, 11, 1, &out);
    benchmark(bogo, 12, 1, &out);
    benchmark(bogo, 13, 1, &out);
  } else {
    std::cerr << "Failed to open/make results file\n";
    return 1;
  }
  out.close();
  if (out.fail()) {
    std::cerr << "Failed to close results file\n";
    return 1;
  }

  delete insertion;
  delete selection;
  delete radix;
  delete bogo;

  std::cout << "Done!\n";
  return 0;
}

void demo(Sorter *sort, int vec_size) {
  std::vector<int> *vec = gen_rand_vec(vec_size);
  std::cout << sort->name << " sorting demonstration on a vector of size "
            << vec_size << std::endl;
  std::cout << "Before:\n";
  std::cout << *vec;
  sort->func(vec);
  std::cout << "After:\n";
  std::cout << *vec;
  return;
}

void init_csv(std::ofstream *out) {
  if (out != NULL) {
    *out << "name,rep,size,time\n";
  }
  return;
}

void benchmark(Sorter *sort, int vec_size, int num_times, std::ofstream *out) {
  using namespace std::chrono;
  std::vector<int> **vecs = new std::vector<int> *[num_times];

  void (*f)(std::vector<int> *) = sort->func;

  for (int i = 0; i < num_times; i++) {
    vecs[i] = gen_rand_vec(vec_size);
  }
  auto start = high_resolution_clock::now();
  for (int i = 0; i < num_times; i++) {
    f(vecs[i]);
  }
  auto end = high_resolution_clock::now();
  for (int i = 0; i < num_times; i++) {
    delete vecs[i];
  }
  auto duration = duration_cast<milliseconds>(end - start);
  delete[] vecs;
  std::cout << "It took " << duration.count() / 1000.00 << " seconds to do "
            << sort->name << " sort " << num_times
            << " times on vectors of size " << vec_size << std::endl;
  if (out != NULL) {
    *out << sort->name << ',' << num_times << ',' << vec_size << ','
         << duration.count() / 1000.00 << std::endl;
  }
  return;
}

std::vector<int> *gen_rand_vec(int vec_size) {
  std::vector<int> *vec = new std::vector<int>;
  for (int i = 0; i < vec_size; i++) {
    vec->push_back(arc4random_uniform(100));
  }
  return vec;
}

void shuffle(std::vector<int> *vec) {
  int index1, index2;
  index1 = arc4random_uniform(vec->size());
  index2 = arc4random_uniform(vec->size());
  if (index1 == index2) {
    return shuffle(vec);
  } else {
    int temp = vec->at(index1);
    vec->at(index1) = vec->at(index2);
    vec->at(index2) = temp;
  }
  return;
}

bool sorted(std::vector<int> vec) {
  bool sorted = true;
  for (size_t i = 0; i < vec.size() - 1; i++) {
    sorted = sorted && vec[i] <= vec[i + 1];
  }
  return sorted;
}

void insertion_sort(std::vector<int> *vec) {
  for (int i = 1; i < (int)vec->size(); i++) {
    int temp = vec->at(i);
    int j;
    for (j = (int)i - 1; j >= 0 && vec->at(j) > temp; j--) {
      vec->at(j + 1) = vec->at(j);
    }
    vec->at(j + 1) = temp;
  }
  return;
}

void selection_sort(std::vector<int> *vec) {
  for (size_t i = 0; i < vec->size(); i++) {
    size_t mini = i;
    for (size_t j = i; j < vec->size(); j++) {
      if (vec->at(j) < vec->at(mini)) {
        mini = j;
      }
    }
    int temp = vec->at(i);
    vec->at(i) = vec->at(mini);
    vec->at(mini) = temp;
  }
  return;
}

void bogo_sort(std::vector<int> *vec) {
  while (!sorted(*vec)) {
    shuffle(vec);
  }
  return;
}

void count_sort(std::vector<int> *vec, int deg) {
  int *output = new int[vec->size()];
  int count[10] = {0};

  for (size_t i = 0; i < vec->size(); i++) {
    count[(vec->at(i) / deg) % 10]++;
  }

  for (size_t i = 1; i < 10; i++) {
    count[i] += count[i - 1];
  }

  for (int i = (int)vec->size() - 1; i >= 0; i--) {
    output[count[(vec->at(i) / deg) % 10] - 1] = vec->at(i);
    count[(vec->at(i) / deg) % 10]--;
  }

  for (size_t i = 0; i < vec->size(); i++) {
    vec->at(i) = output[i];
  }
  delete[] output;
  return;
}

void radix_sort(std::vector<int> *vec) {
  int max = *std::max_element(vec->begin(), vec->end());
  for (int i = 1; max / i > 0; i *= 10) {
    count_sort(vec, i);
  }
  return;
}
