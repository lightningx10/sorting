# Sorting benchmarks

This is a crude collection of sorting algorithms written in different languages.
This is just for fun, they are not as efficient as possible but they are as
faithful as I can reasonably accomplish in each language.

The purpose of this repository is to provide insights into each language, to
compare sorting algorithms, and to educate myself on how these sorting functions
work.

This is public domain, do with it what you want.
