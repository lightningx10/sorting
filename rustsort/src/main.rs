use rand::prelude::*;

extern crate time;
use time::Instant;

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

fn gen_rand_vec(len: usize) -> Vec<i32> {
    let mut rng = rand::thread_rng();
    let mut vec = Vec::with_capacity(len);
    for _ in 0..len {
        vec.push((rng.gen::<f64>() * 100.00).round() as i32);
    }
    vec
}

fn shuffle(vec: &mut Vec<i32>) {
    let mut rng = rand::thread_rng();
    let index1 = (rng.gen::<f64>() * (vec.len() - 1) as f64).round() as usize;
    let index2 = (rng.gen::<f64>() * (vec.len() - 1) as f64).round() as usize;
    if index1 == index2 {
        shuffle(vec);
    } else {
        let temp = vec[index1];
        vec[index1] = vec[index2];
        vec[index2] = temp;
    }
}

fn sorted(vec: &Vec<i32>) -> bool {
    let mut sorted = true;
    for i in 0..vec.len() - 1 {
        sorted = sorted && vec[i] <= vec[i + 1];
    }
    sorted
}

fn bogo_sort(vec: &mut Vec<i32>) {
    while !sorted(vec) {
        shuffle(vec);
    }
}

fn insertion_sort(vec: &mut Vec<i32>) {
    for i in 1..vec.len() {
        let temp = vec[i];
        let mut j = i - 1;
        let mut stop = false;
        while !stop && vec[j] > temp {
            vec[j + 1] = vec[j];
            if j == 0 {
                stop = true;
            } else {
                j -= 1;
            }
        }
        vec[j] = temp;
    }
}

fn selection_sort(vec: &mut Vec<i32>) {
    for i in 0..vec.len() {
        let mut mini = i;
        for j in i..vec.len() {
            if vec[j] < vec[mini] {
                mini = j;
            }
        }
        let temp = vec[i];
        vec[i] = vec[mini];
        vec[mini] = temp;
    }
}

fn get_max(vec: &mut Vec<i32>) -> i32 {
    let mut max = vec[0];
    for i in 0..vec.len() {
        if vec[i] > max {
            max = vec[i];
        }
    }
    max
}

fn count_sort(vec: &mut Vec<i32>, deg: i32) {
    let mut output = Vec::with_capacity(vec.len());
    let mut count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for i in 0..vec.len() {
        output.push(0);
        count[((vec[i] / deg) % 10) as usize] += 1;
    }
    for i in 1..10 {
        count[i] += count[i - 1];
    }
    for i in (0..=vec.len() - 1).rev() {
        output[(count[((vec[i] / deg) % 10) as usize] - 1) as usize] = vec[i];
        count[((vec[i] / deg) % 10) as usize] -= 1;
    }
    for i in 0..vec.len() {
        vec[i] = output[i];
    }
}

fn radix_sort(vec: &mut Vec<i32>) {
    let max = get_max(vec);
    let mut i = 1;
    while max / i > 0 {
        count_sort(vec, i);
        i *= 10;
    }
}

fn demo(f: &dyn Fn(&mut Vec<i32>), vec_size: usize, name: &str) {
    println!(
        "{} sorting demonstration on an array of size {}:",
        name, vec_size
    );
    let mut vec = gen_rand_vec(vec_size);
    println!("Before:");
    println!("{:?}", vec);
    f(&mut vec);
    println!("After:");
    println!("{:?}", vec);
}

fn init_csv(file: &mut File) {
    match file.write_fmt(format_args!("name,rep,size,time\n")) {
        Err(why) => panic!("Failed to write to results file\n{}", why),
        Ok(_) => (),
    };
}

fn benchmark(
    f: &dyn Fn(&mut Vec<i32>),
    vec_size: usize,
    num_times: usize,
    name: &str,
    file: &mut File,
) {
    let mut vecs: Vec<Vec<i32>> = vec![vec![0; vec_size]; num_times];
    for i in 0..num_times {
        vecs[i] = gen_rand_vec(vec_size);
    }
    let start = Instant::now();
    for i in 0..num_times {
        f(&mut vecs[i]);
    }
    let t = start.elapsed();
    println!(
        "It took {} seconds to do {} sort {} times on arrays of size {}",
        t.as_seconds_f64(),
        name,
        num_times,
        vec_size
    );
    match file.write_fmt(format_args!(
        "{},{},{},{}\n",
        name,
        num_times,
        vec_size,
        t.as_seconds_f64()
    )) {
        Err(why) => panic!("Failed to write to results file\n{}", why),
        Ok(_) => (),
    };
}

fn main() {
    println!("Starting sorting demonstrations in Rust");
    demo(&insertion_sort, 10, "Insertion");
    demo(&selection_sort, 10, "Selection");
    demo(&radix_sort, 10, "Radix");
    demo(&bogo_sort, 11, "Bogo");
    demo(&bogo_sort, 12, "Bogo");

    println!("Starting timed sorting in Rust");
    let path = Path::new("results/rust.csv");
    let mut file = match File::create(&path) {
        Err(why) => panic!("Failed to make results file\n{}", why),
        Ok(file) => file,
    };
    init_csv(&mut file);
    benchmark(&insertion_sort, 1200, 10000, "Insertion", &mut file);
    benchmark(&insertion_sort, 5000, 10000, "Insertion", &mut file);
    benchmark(&insertion_sort, 12000, 10000, "Insertion", &mut file);
    benchmark(&selection_sort, 1200, 10000, "Selection", &mut file);
    benchmark(&selection_sort, 5000, 10000, "Selection", &mut file);
    benchmark(&selection_sort, 12000, 10000, "Selection", &mut file);
    benchmark(&radix_sort, 1200, 10000, "Radix", &mut file);
    benchmark(&radix_sort, 5000, 10000, "Radix", &mut file);
    benchmark(&radix_sort, 12000, 10000, "Radix", &mut file);
    benchmark(&bogo_sort, 11, 1, "Bogo", &mut file);
    benchmark(&bogo_sort, 12, 1, "Bogo", &mut file);
    benchmark(&bogo_sort, 13, 1, "Bogo", &mut file);

    println!("Done!");
}
